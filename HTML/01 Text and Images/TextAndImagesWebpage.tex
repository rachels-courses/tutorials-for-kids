\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}
\newcommand{\laTopic}       {Text and images in a webpage}
\newcommand{\laTitle}       {Rachel's Tutorials}
\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}
\title{Introduction to C++}
\author{Rachel Singh}
\date{\today}
\pagestyle{fancy}
\fancyhf{}
\lhead{\laTopic \ / \laTitle}
\chead{}
\rhead{\thepage}
\rfoot{\tiny \thepage\ of \pageref{LastPage}}
\lfoot{\tiny Rachel Singh, last updated \today}
\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}
\renewcommand{\chaptername}{Topic}

\begin{document}
    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont Web Tutorial:} ~\\~\\
                {\fontsize{2cm}{3cm}\selectfont \laTopic}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{images/webpage.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                Written by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{../../content/other/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}
    
    %\tableofcontents
    
    \section*{Setup}
    
    ~\\ 1. Go to the https://repl.it website and click on ``Sign in''. Log in to your repl.it account.
    
    ~\\ 2. Search for the language ``HTML, CSS, and JS'' and then click ``Create repl''.
    
    ~\\ 3. The center column is where you write code, the right column is where your website will show up when you click ``Run''.
    
    \begin{center}
        \includegraphics[width=14cm]{images/00.png}
    \end{center}
    
    \hrulefill
    \section*{What is HTML?}
        When we're building a webpage, we need some way to tell the
        internet browser (like Firefox or Chrome) what kind of text
        is on the page, what kind of images are on the page, and
        what kind of links are on the page. Webpages are built with
        a text language called ``HTML'' - HyperText Markup Language.
        
        \newpage
        \subsection*{An empty webpage}
        At minimum, a webpage should have this starting out:

\begin{lstlisting}[style=code]
<html>
    <head></head>
    
    <body>
    
    </body>
</html>
\end{lstlisting}

        HTML is made up of \textbf{tags}. Tags are those special
        markings within the $<$ and $>$ signs, which indicate what
        is going to be inside.
        
        \paragraph{$<$html$>$:} We start a webpage with \texttt{<html>} and end it with \texttt{</html>}.
        
        \paragraph{$<$head$>$:} The head tag is to store information \textit{about} the webpage. Nothing up here is actually drawn to the webpage. We will cover this more later on.
        
        \paragraph{$<$body$>$:} The actual webpage code will go within the \texttt{<body>} tags. The end of the webpage view is \texttt{</body>}.
        
        \newpage
        \subsection*{Writing text}
        
        To write basic text to our webpage, we use the \textbf{paragraph tag}, which is \texttt{<p> </p>}.
        Text can go within these tags to separate them off into separate paragraphs.
        
        Even if you put new lines in your HTML code, it won't show up in the webpage.
        You have to use paragraph tags to separate out the text.
        
\begin{lstlisting}[style=htmlcode]
<html>
  <head></head>
  <body>
    <p>
      We can dance if we want to.
      We can leave your friends behind.
      Cause' your friends don't dance,
      and if they don't dance, well they're
      no friends of mine.
    </p>
    <p>
      I say, we can go where we want to.
      A place where they will never find.
      And we can act like we come from out of this world
      Leave the real one far behind.
    </p>
  </body>
</html>
\end{lstlisting}

        \begin{center}
            \begin{mdframed}
            \includegraphics[width=13cm]{images/01.png}
            \end{mdframed}
        \end{center}
        
        
        
        \newpage
        \section*{Display images}
        To display an image, you will need to have your image somewhere
        on the internet. On repl.it, you can upload an image to your project.
        On the left side of the view is the \textbf{Files} view. Click on the ``...''
        button, and choose ``Upload file''.
        
        \begin{center}
            \includegraphics[width=10cm]{images/02.png}
        \end{center}
        
        After the file is uploaded, look at the same of your file. For example,
        my image is ``safety.png''.
        
        We're going to use an \textbf{image tag} to add an image to our page.
        The tag name is \texttt{img}, and we need to have an attribute called
        \texttt{src} to specify the name of the image we want to insert.
        
    
\begin{lstlisting}[style=htmlcode]
<img src="rawr.png">
\end{lstlisting}     
        
        \newpage
        
\begin{lstlisting}[style=htmlcode]
<html>
  <head></head>
  <body>
    <img src="safety.png">
    <p>
      We can dance if we want to.
      We can leave your friends behind.
      Cause' your friends don't dance,
      and if they don't dance, well they're
      no friends of mine.
    </p>
    <p>
      I say, we can go where we want to.
      A place where they will never find.
      And we can act like we come from out of this world
      Leave the real one far behind.
    </p>
  </body>
</html>
\end{lstlisting}
        
        \begin{center}
            \begin{mdframed}\includegraphics[width=9cm]{images/03.png}\end{mdframed}
        \end{center}
        
        
        \newpage
        \section*{Headers}
        Header tags are a way we can categorize our page, similar to how you
        would organize sections in a textbook. The top-most header is
        \texttt{<h1></h1>}, usually for the name of your page or what
        your overall page is about, with \texttt{<h2></h2>} being for
        sub-topics, all the way down to \texttt{<h6></h6>}.
        
\begin{lstlisting}[style=htmlcode]
<html>
  <head></head>
  <body>
    <h1>Safety Dance</h1>
    <img src="safety.png">

    <h2>Lyrics</h2>
    <p>
      We can dance if we want to.
      We can leave your friends behind.
      Cause' your friends don't dance,
      and if they don't dance, well they're
      no friends of mine.
    </p>
    <p>
      I say, we can go where we want to.
      A place where they will never find.
      And we can act like we come from out of this world
      Leave the real one far behind.
    </p>
  </body>
</html>

\end{lstlisting}
        
        \begin{center}
            \begin{mdframed}\includegraphics[width=9cm]{images/04.png}\end{mdframed}
        \end{center}
        
        
\end{document}

