print( "THE BURRITO" )
print( "" )
print( "There was once a girl who wanted a burrito." )
print( "She decided to go to the fridge to look for ingredients." )
print( "There were no tortillas!" )
print( "So the girl went to the store." )
print( "" )
print( "The girl went to the tortilla aisle." )
print( "But there were no tortillas there, either!" )
print( "The girl went home, sad and heartbroken." )
print( "" )
print( "THE END" )
