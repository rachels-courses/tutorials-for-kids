\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}
\newcommand{\laTopic}       {Writing a recipe program in Python}
\newcommand{\laTitle}       {Rachel's Tutorials}
\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}
\title{Introduction to C++}
\author{Rachel Singh}
\date{\today}
\pagestyle{fancy}
\fancyhf{}
\lhead{\laTopic \ / \laTitle}
\chead{}
\rhead{\thepage}
\rfoot{\tiny \thepage\ of \pageref{LastPage}}
\lfoot{\tiny Rachel Singh, last updated \today}
\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}
\renewcommand{\chaptername}{Topic}

\begin{document}
    \begin{titlepage}
        \centering{}

        \sffamily{
            \textbf{
                {\fontsize{2cm}{3cm}\selectfont Python Tutorial:}~\\~\\
                {\fontsize{2cm}{3cm}\selectfont \laTopic}
            }
        }

        \begin{figure}[h]
            \begin{center}
                \includegraphics[width=12cm]{images/python.png}
            \end{center}
        \end{figure}

        \sffamily{
            \textbf{
                Written by Rachel Singh
            }
        }
        
        \vspace{1cm} \small
        This work is licensed under a \\ Creative Commons Attribution 4.0 International License. ~\\~\\
        \includegraphics{../../content/other/cc-by-88x31.png}
        ~\\~\\
        
        Last updated \today

    \end{titlepage}
    
    %\tableofcontents
    
    \section*{Setup}
    
        ~\\ 1. Go to the https://repl.it website and click on ``Sign in''. Log in to your repl.it account.
        
        ~\\ 2. Search for the language Python and then click Create repl
        
        ~\\ 3. The center column (labelled main.py) is the code window, and the right column is the terminal, where you can see your program running.
        
        \begin{center}
            \includegraphics{images/2020-09-24_repl-view.png}
        \end{center}
    
    \newpage
    \section*{Challenge 1: Output a recipe}
    
        \begin{center}
            \includegraphics[width=0.2\textwidth]{images/cookierecipe.png}
        \end{center}
        
        \subsection*{Step 1: Creating ingredient variables}
        First, we're going to create \textbf{variables} to store the amount 
        of each ingredient we need for our recipe. This way, we could change up
        the ingredient amounts quickly and all in one place if we need to adjust
        the recipe later.
        
        \begin{intro}{What is a variable?}
        
            \begin{wrapfigure}{r}{0.2\textwidth}
              \begin{center}
                \includegraphics[width=0.2\textwidth]{images/variable.png}
              \end{center}
            \end{wrapfigure}
            
            A \textbf{variable} is a place where we can store data in a program.
            It can store any kind of data, and we can retrieve it later.
            
            
            ~\\ \textbf{Examples of variables:}
            \begin{itemize}
                \item   The player's amount of \textbf{money} in a video game.
                \item   The number of \textbf{eggs} to use in a recipe.
                \item   Your \textbf{name} in a video game.
            \end{itemize}
        \end{intro}
        ~\\First step for our recipe program is to create \textbf{variables} for
        each of the ingredients.
        
        ~\\ To make a \textbf{variable} and store a number inside it,
        we come up with a \textbf{variable name}, which can be letters and numbers and the underscore,
        but no spaces. 
        
        ~\\ Then, we use the equal sign = to say ``we're going to store something in this variable''.
        
        ~\\ Finally, we put a number (or, if we wanted, other data) after the = sign
        to store it in there. For example:
        
\begin{lstlisting}[style=pycode]
brownsugar = 0.75
eggs = 2
\end{lstlisting}

        \newpage
        We're going to need \textbf{variables} to store each of the
        ingredients for cookies, so it will look like this:

\begin{lstlisting}[style=pycode]
# Set up ingredients
flour = 3
brownsugar = 0.75
eggs = 2
butter = 1
chocolatechips = 1
\end{lstlisting}

        \begin{intro}{What is the \#?}
            In Python, we can write a \textbf{comment} in the code by
            putting a \# before any text. This is meant to be a note to
            ourselves or to other people. The computer won't try to 
            read any \textbf{comments} - it's just for humans!
        \end{intro}
        
        \hrulefill
        
        \subsection*{Step 2: Displaying the ingredients}
        Next we will use the \texttt{print()} function to display
        the cookie recipe to the screen. Using the \texttt{print()}
        function will look like this:
        
        \begin{center}
            \large
            \texttt{ print("Hello!") }
        \end{center}
        
        Make sure you have both parentheses ( ) and both double-quotes \texttt{"} \texttt{"}, with your text inside!
        ~\\~\\
        The first part of our recipe will be... to tell the user what
        this is a recipe for!

\begin{lstlisting}[style=pycode]
# Display recipe
print( "CHOCOLATE CHIP COOKIE RECIPE" )
\end{lstlisting}
        
        \newpage
        Next, we will want to display each of the ingredients to the screen.
        We will keep using the \texttt{print()} function, but we need to
        print the \textbf{variable} AND some text.
        ~\\
        
        We can use \texttt{print()} with just a variable name, like this:
        \texttt{print(flour)} - however, this will ONLY display the number
        we stored in this \textbf{variable} - it won't tell the user
        that this should be ``cups of flour''. We need to put that ourselves.
        
\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
    
    \textbf{Python code:}
\begin{lstlisting}[style=pycode]
print(flour)
\end{lstlisting}
           
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}

    \textbf{Program output:}
\begin{lstlisting}[style=output]
3
\end{lstlisting}

    \end{subfigure}
\end{figure}

        Instead, we can combine a \textbf{variable} and some \textbf{text}
        within our \texttt{print()} function to display more information:

        ~\\ \textbf{Python code:}
\begin{lstlisting}[style=pycode]
print( flour, "cups flour" )
\end{lstlisting}

        ~\\ \textbf{Program output:}
\begin{lstlisting}[style=output]
3 cups flour
\end{lstlisting}
        ~\\
        
        So to display all the ingredients, we do this:
    
\begin{lstlisting}[style=pycode]
print( flour, "cups flour" )
print( brownsugar, "cups brown sugar" )
print( eggs, "eggs" )
print( butter, "cups butter" )
print( chocolatechips, "cups chocolate chips" )
\end{lstlisting}

        ~\\
        
        On the next page is the entire recipe program.
        
        \newpage
        \paragraph{Full program:}
        \begin{center}
            \includegraphics[width=14cm]{images/recipeonrepl.png}
        \end{center}
        
        \paragraph{Program output:}
        \begin{center}
            \includegraphics[width=10cm]{images/recipeonrepl2.png}
        \end{center}
        
    \newpage
    \section*{Challenge 2: Setting batches}
    
        Next, we'll ask the user for how many \textbf{batches} of cookies
        they want to make ahead of time, and then we'll adjust the
        recipe based on their input. We just need to add one line
        of code and slightly modify our ingredient \textbf{variables}.
        
        \subsection*{Step 1: Asking how many batches}
        
            We will use the \texttt{input()} function to ask the user
            how many batches they want to make, and store their answer
            in a \textbf{variable} that we can do math with.
            ~\\
            
            The input function has several parts:
            ~\\~\\
            \begin{tabular}{c c c c c c}
                \texttt{batches} & \texttt{=} & \texttt{float( } & \texttt{input(} & \texttt{ "How many batches of cookies?" } & \texttt{ ) ) }
                \\
                \footnotesize variable & \footnotesize  & \footnotesize type & \footnotesize input & \footnotesize question message & \footnotesize end
            \end{tabular}
            ~\\
            
            This code will go \underline{at the very beginning of the program},
            before we make our \textbf{variables}.
            
\begin{lstlisting}[style=pycode]
# Ask how many batches
batches = float(input("How many batches of cookies? "))
\end{lstlisting}

            \begin{intro}{What is a float?}
                In program code, we need to tell the computer \textit{what kind of data}
                is going to be stored. There are many types, but here are a few:
                
                \begin{itemize}
                    \item   \textbf{int}: An integer, which is a whole number - no decimal places.
                    \item   \textbf{float}: A number that can have a decimal place.
                \end{itemize}
            \end{intro}

        \newpage
        \subsection*{Step 2: Updating ingredients}
        
            Next, we will update the ingredient \textbf{variables}.
            If we were going to use 2 eggs originally, and the user wants
            to make a half batch, then the amount of eggs for a half batch is
            just 1. We can calculate this with:
            
            \begin{center}
                \large
                \texttt{ eggs = 2 * batches }
            \end{center}
            
            All of the ingredients will need to be updated to use a 
            simple \textbf{formula} to figure out how many of each ingredient...
        
\begin{lstlisting}[style=pycode]
# Set up ingredients
flour = 3 * batches
brownsugar = 0.75 * batches
eggs = 2 * batches
butter = 1 * batches
chocolatechips = 1 * batches
\end{lstlisting}

            The rest of the program can stay the same.
            
            ~\\
            See the next page for the full program.
            
            \vspace{2cm}
            
            \begin{center}
                \includegraphics[width=10cm]{images/cookies.png}
            \end{center}
                
            \newpage
            \paragraph{Full program:}
            \begin{center}
                \includegraphics[width=14cm]{images/recipeonrepl3.png}
            \end{center}
            
            \paragraph{Program output:}
            \begin{center}
                \includegraphics[width=10cm]{images/recipeonrepl4.png}
            \end{center}

\end{document}

