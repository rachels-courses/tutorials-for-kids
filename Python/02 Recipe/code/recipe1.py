# Set up ingredients
flour = 3
brownsugar = 0.75
eggs = 2
butter = 1
chocolatechips = 1

# Display recipe
print( "CHOCOLATE CHIP COOKIE RECIPE" )

print( flour, "cups flour" )
print( brownsugar, "cups brown sugar" )
print( eggs, "eggs" )
print( butter, "cups butter" )
print( chocolatechips, "cups chocolate chips" )