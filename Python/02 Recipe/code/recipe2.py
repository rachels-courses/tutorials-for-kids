# Ask how many batches
batches = input( "How many batches do you want to make? " )

# Set up ingredients
flour = 3 * batches
brownsugar = 0.75 * batches
eggs = 2 * batches
butter = 1 * batches
chocolatechips = 1 * batches

# Display recipe
print( "CHOCOLATE CHIP COOKIE RECIPE" )

print( flour, "cups flour" )
print( brownsugar, "cups brown sugar" )
print( eggs, "eggs" )
print( butter, "cups butter" )
print( chocolatechips, "cups chocolate chips" )